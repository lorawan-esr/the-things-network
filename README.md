# The Things Network

[The Things Network](https://www.thethingsnetwork.org/) est un réseau [communautaire](https://www.thethingsnetwork.org/community) et open source mondiale pour l'Internet des Objets. Ce réseau est basé sur la technologie LoRa. Il permet aux [membres](https://www.thethingsnetwork.org/community/montpellier/) de la communauté de déployer des passerelles ou simplement d'utiliser les passerelles existantes pour transmettre les données de leurs objets connectés.

Ce dépôt contient les ressources nécessaires pour connecter vos objets en utilisant LoRa et The Things Network.

## Node-RED

[Node-RED](https://nodered.org/) est un outil de programmation visuel permettant de connecter des périphériques matériels, des API et des services en ligne.

Le flux (ou flow) Node-RED ci-dessous récupére les données provenant d'un capteur de température et d'humidité via The Things Network puis les insère dans une base de données MySQL.

Pour récupérer les données de vos capteurs via The Things Network vous pouvez importer le fichier [node-red_flow_ttn-mysql.json](https://forgemia.inra.fr/lorawan-esr/the-things-network/-/blob/main/node-red_flow_ttn-mysql.json) dans Node-RED. Renseignez ensuite les informations pour The Things Network et votre base de données MySQL.

![Flow Node-RED](https://forgemia.inra.fr/lorawan-esr/the-things-network/-/raw/main/source/images/images/node-red_flow_ttn-mysql.png){width=75%}


